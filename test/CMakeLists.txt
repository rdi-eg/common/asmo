rdi_external_project_add(rdi_wfile
	GIT_REPO   "git@arwash.rdi:common/wfile.git"
	PATH       ${workspace_location}/common/wfile
	GIT_TAG    v1.0
)

add_executable(test
	main.cpp
	tests.cpp
)

target_link_libraries(test
	PRIVATE
	    rdi_asmo
		rdi_wfile
)

target_compile_definitions(test PRIVATE
"-DCODE_LOCATION=\"${CMAKE_SOURCE_DIR}/test/\""
)

include(${cmake_scripts_path}/rdi_download_unittest_header.cmake)
download_catch(${CMAKE_SOURCE_DIR}/test)
