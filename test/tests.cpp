#include <iostream>
#include <string>
#include "catch.hpp"

#include <rdi_asmo.hpp>
#include <rdi_wfile.hpp>

using namespace RDI;
using namespace  std;

const string test_path = string(CODE_LOCATION);
TEST_CASE("arabic_to_asmo")
{
	vector<wstring> arabic = read_wfile_lines(test_path + "tests/GT/input.txt");
	vector<string> expected_asmo = read_file_lines(test_path + "tests/GT/output.txt");

	vector<string> actual_asmo(arabic.size());
	for ( size_t i = 0 ; i < arabic.size() ; i++ )
	{

		wstring temp = ASMO::prepare_numbers_and_puncs(arabic[i]);
		actual_asmo[i] = ASMO::convert_arabic_to_asmo(temp);
        if ( expected_asmo[i].back() == '\r' )
        {
            expected_asmo[i].pop_back();
        }
		CHECK( actual_asmo[i] == expected_asmo[i]);
	}
}
