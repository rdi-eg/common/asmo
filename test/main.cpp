#define CATCH_CONFIG_RUNNER
#define TEST true
#include "catch.hpp"

int main( int argc, char* const argv[] )
{
    //If the TEST macro is defined to be true,
    //runCatchTests will be called and immediately
    //return causing the program to terminate. Change TEST
    //to false in the macro def at the top of this file
    //to skip tests and run the rest of your code.
    if (TEST)
    {
        return Catch::Session().run(argc, argv);
    }

    //start working on other parts of your project here.
    return 0;
}
