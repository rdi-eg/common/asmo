include(${cmake_scripts_path}/rdi_external_project_add.cmake)

add_library(rdi_asmo STATIC
	rdi_asmo.hpp
	asmo.cpp
)

target_include_directories(rdi_asmo INTERFACE
	${CMAKE_CURRENT_SOURCE_DIR}
)
