#include "rdi_asmo.hpp"
#include <algorithm>
#include <vector>
#include <string>

#if __GNUC__ >= 7 || __cplusplus >= 2017
#include <optional>
#define OPTIONAL std::optional
#define NULLOPT std::nullopt

#elif __has_include(<experimental/optional>)
# include <experimental/optional>
#define OPTIONAL std::experimental::optional
#define NULLOPT std::experimental::nullopt

#else
#   error Must have an optional type, either from <optional> or if not supported from <experimental/optional>.
#endif
namespace RDI
{

namespace ASMO
{

extern const std::vector<std::pair<wchar_t, char>> arabic_asmo_numbers{
	// nubmers
	{ (wchar_t)ar::sefr, '0' },      { (wchar_t)ar::wahed, '1' },
	{ (wchar_t)ar::ethnan, '2' },    { (wchar_t)ar::thalatha, '3' },
	{ (wchar_t)ar::arba3a, '4' },    { (wchar_t)ar::khamsa, '5' },
	{ (wchar_t)ar::setta, '6' },     { (wchar_t)ar::sab3a, '7' },
	{ (wchar_t)ar::thamaneya, '8' }, { (wchar_t)ar::tes3a, '9' } 
};

extern const std::vector<std::pair<wchar_t, char>> arabic_asmo_symbols{
	// symbols
	{ (wchar_t)ar::fasla, ',' },          { (wchar_t)ar::question_mark, '?' },
	{ (wchar_t)ar::fasla_mankoota, ';' }, { (wchar_t)ar::noqtatan, ':' },
	{ (wchar_t)ar::nesba, '%' },          { (wchar_t)ar::ta3agob, '!' },
	{ (wchar_t)ar::qaws_maftooh, '(' },   { (wchar_t)ar::qaws_makfool, ')' },
	{ (wchar_t)ar::darb, '*' },           { (wchar_t)ar::jam3, '+' },
	{ (wchar_t)ar::kesma, '/' },          { (wchar_t)ar::tar7, '-' },
	{ (wchar_t)ar::yosawi, '-' },         { (wchar_t)ar::noqta, '.' },
    { (wchar_t)ar::tansees, '"' },        { (wchar_t)ar::yosawi, '=' }
};

extern const std::vector<std::pair<wchar_t, char>> arabic_asmo_chars{
	// alphabet
	{ (wchar_t)ar::hamza, 'A' },
	{ (wchar_t)ar::alef_madda, 'B' },
	{ (wchar_t)ar::alef_hamza_above, 'C' },
	{ (wchar_t)ar::waw_hamza_above, 'D' },
	{ (wchar_t)ar::alef_hamza_below, 'E' },
	{ (wchar_t)ar::yeh_hamza_above, 'F' },
	{ (wchar_t)ar::alef_no_hamza, 'G' },
	{ (wchar_t)ar::beh, 'H' },
	{ (wchar_t)ar::teh_marboota, 'I' },
	{ (wchar_t)ar::teh, 'J' },
	{ (wchar_t)ar::theh, 'K' },
	{ (wchar_t)ar::jeem, 'L' },
	{ (wchar_t)ar::hah, 'M' },
	{ (wchar_t)ar::khah, 'N' },
	{ (wchar_t)ar::dal, 'O' },
	{ (wchar_t)ar::thal, 'P' },
	{ (wchar_t)ar::reh, 'Q' },
	{ (wchar_t)ar::zain, 'R' },
	{ (wchar_t)ar::seen, 'S' },
	{ (wchar_t)ar::sheen, 'T' },
	{ (wchar_t)ar::sad, 'U' },
	{ (wchar_t)ar::dad, 'V' },
	{ (wchar_t)ar::tah, 'W' },
	{ (wchar_t)ar::zah, 'X' },
	{ (wchar_t)ar::ain, 'Y' },
	{ (wchar_t)ar::ghain, 'Z' },
	{ (wchar_t)ar::feh, 'a' },
	{ (wchar_t)ar::qaf, 'b' },
	{ (wchar_t)ar::kaf, 'c' },
	{ (wchar_t)ar::lam, 'd' },
	{ (wchar_t)ar::meem, 'e' },
	{ (wchar_t)ar::noon, 'f' },
	{ (wchar_t)ar::heh, 'g' },
	{ (wchar_t)ar::waw, 'h' },
	{ (wchar_t)ar::yeh_no_dots, 'i' },
	{ (wchar_t)ar::yeh_with_dots, 'j' }
};

extern const std::vector<std::pair<wchar_t, std::string>> arabic_str_asmo_numbers{
	// nubmers
	{ (wchar_t)ar::sefr, "L191" },      { (wchar_t)ar::wahed, "L192" },
	{ (wchar_t)ar::ethnan, "L193" },    { (wchar_t)ar::thalatha, "L194" },
	{ (wchar_t)ar::arba3a, "L195" },    { (wchar_t)ar::khamsa, "L196" },
	{ (wchar_t)ar::setta, "L197" },     { (wchar_t)ar::sab3a, "L198" },
	{ (wchar_t)ar::thamaneya, "L199" }, { (wchar_t)ar::tes3a, "L200" }
};

extern const std::vector<std::pair<wchar_t, std::string>> arabic_str_asmo_symbols{
	// symbols

	{ (wchar_t)ar::kesma, "S15" },          { (wchar_t)ar::tar7, "S11" },
	{ (wchar_t)ar::yosawi, "S13" }     
};


OPTIONAL<wchar_t> get_arabic_from_vector(char c,
                                              const std::vector<std::pair<wchar_t, char>>& v)
{
	auto it = std::find_if(v.begin(), v.end(), [&](const std::pair<wchar_t, char>& element) {
		return element.second == c;
	});
	if (it == v.end())
		return NULLOPT;
	return it->first;
}

OPTIONAL<wchar_t> get_arabic_from_vector(std::string c, 
	const std::vector<std::pair<wchar_t, std::string>>& v)

{

	auto it = std::find_if(v.begin(), v.end(), [&](const std::pair<wchar_t, std::string>& element) {
		return (element.second + " " == c || element.second == c); /*sometimes the decodeoutput has a space at its end*/
		});
	if (it == v.end())
		return NULLOPT;
	return it->first;
}


OPTIONAL<char> get_asmo_from_vector(wchar_t c,
                                         const std::vector<std::pair<wchar_t, char>>& v)
{
	auto it = std::find_if(v.begin(), v.end(), [&](const std::pair<wchar_t, char>& element) {
		return element.first == c;
	});

	if (it == v.end())
		return NULLOPT;
	return it->second;
}

OPTIONAL<std::string> get_asmo_from_vector(wchar_t c,
                                         const std::vector<std::pair<wchar_t, std::string>>& v)
{
	auto it = std::find_if(v.begin(), v.end(), [&](const std::pair<wchar_t, std::string>& element) {
		return element.first == c;
	});

	if (it == v.end())
		return NULLOPT;
	return it->second;
}

bool is_arabic_symbol(wchar_t c)
{
	OPTIONAL<char> symbol;
	OPTIONAL<std::string> symbol_str;

	symbol = get_asmo_from_vector(c, arabic_asmo_symbols);
	if (symbol)
	{
		return true;
	}
	
	symbol_str = get_asmo_from_vector(c, arabic_str_asmo_symbols);
	if (symbol)
	{
		return true;
	}
	return false;
}

bool is_arabic_digit(wchar_t c)
{
	OPTIONAL<char> number;
	OPTIONAL<std::string> number_str;

	number = get_asmo_from_vector(c, arabic_asmo_numbers);
	if (number)
	{
		return true;
	}
	number_str = get_asmo_from_vector(c, arabic_str_asmo_numbers);
	
	if (number_str)
	{
		return true;
	}
	return false;
}

bool is_arabic_char(wchar_t c)
{
	OPTIONAL<char> ch;

	ch = get_asmo_from_vector(c, arabic_asmo_chars);
	if (ch)
	{
		return true;
	}
	return false;
}

bool is_asmo_symbol(char c)
{
	OPTIONAL<wchar_t> symbol;

	symbol = get_arabic_from_vector(c, arabic_asmo_symbols);
	if (symbol)
	{
		return true;
	}
	return false;
}

bool is_asmo_symbol(std::string c)
{
	OPTIONAL<wchar_t> symbol;

	symbol = get_arabic_from_vector(c, arabic_str_asmo_symbols);
	if (symbol)
	{
		return true;
	}
	return false;
}

bool is_asmo_digit(char c)
{
	OPTIONAL<wchar_t> number;

	number = get_arabic_from_vector(c, arabic_asmo_numbers);
	if (number)
	{
		return true;
	}
	return false;
}

bool is_asmo_digit(std::string c)
{
	OPTIONAL<wchar_t> number;

	number = get_arabic_from_vector(c, arabic_str_asmo_numbers);
	if (number)
	{
		return true;
	}
	return false;
}

bool is_asmo_digit(wchar_t c)
{
//	if ((c >> 8) > 0) // if contains more than a byte
//	{
//		return false;
//	}
	int n = c - L'0';
	char test = n + '0';

	OPTIONAL<wchar_t> number;

	number = get_arabic_from_vector(test, arabic_asmo_numbers);
	if (number)
	{
		return true;
	}
	return false;
}

bool is_asmo_char(wchar_t c)
{
	OPTIONAL<wchar_t> ch;

	ch = get_arabic_from_vector(c, arabic_asmo_chars);
	if (ch)
	{
		return true;
	}
	return false;
}

std::wstring handle_tashkeel(const std::wstring input, int tn_flag, int shada_flag, int tashkeel_flag)
{
	std::wstring tashkeeled_output = input;
	//std::wstring addi = (wchar_t)ar::waw; 

	if (tashkeel_flag == 1)
	{
		tashkeeled_output += (wchar_t)ar::skoon;
		return tashkeeled_output;
	}

	if (shada_flag)
	{
		tashkeeled_output += (wchar_t)ar::shada;
	}

	if (tn_flag)
	{

		 if (tashkeel_flag == 2)
		{
			tashkeeled_output += (wchar_t)ar::tanween_fatha;
		}
		else if (tashkeel_flag == 3)
		{
			tashkeeled_output += (wchar_t)ar::tanween_dama;
		}
		else if (tashkeel_flag == 4)
		{
			tashkeeled_output += (wchar_t)ar::tanween_kasra;
		}
	}
	else
	{
	
		 if (tashkeel_flag == 2)
		{
			tashkeeled_output += (wchar_t)ar::fatha;
		}
		else if (tashkeel_flag == 3)
		{
			tashkeeled_output += (wchar_t)ar::dama;
		}
		else if (tashkeel_flag == 4)
		{
			tashkeeled_output += (wchar_t)ar::kasra;
		}
	}

	return tashkeeled_output; 
}

OPTIONAL<char> arabic_char_to_asmo(wchar_t wc)
{
	OPTIONAL<char> ret;

	ret = get_asmo_from_vector(wc, arabic_asmo_chars);
	if (ret)
	{
		return ret;
	}

	ret = get_asmo_from_vector(wc, arabic_asmo_symbols);
	if (ret)
	{
		return ret;
	}

	ret = get_asmo_from_vector(wc, arabic_asmo_numbers);
	if (ret)
	{
		return ret;
	}

	if (is_asmo_digit(wc))
	{
		int n = wc - L'0';
		ret   = n + '0';
		return ret;
	}

	return NULLOPT;
}

OPTIONAL<std::string> arabic_char_to_asmo_str(wchar_t wc)
{
	OPTIONAL<std::string> ret;

	ret = get_asmo_from_vector(wc, arabic_str_asmo_numbers);
	if (ret)
	{
		return ret;
	}

	ret = get_asmo_from_vector(wc, arabic_str_asmo_symbols);
	if (ret)
	{
		return ret;
	}
	return NULLOPT;
}


OPTIONAL<wchar_t> asmo_char_to_arabic(char c)
{
	OPTIONAL<wchar_t> ret;

	ret = get_arabic_from_vector(c, arabic_asmo_chars);
	if (ret)
	{
		return ret;
	}

	ret = get_arabic_from_vector(c, arabic_asmo_symbols);
	if (ret)
	{
		return ret;
	}

	ret = get_arabic_from_vector(c, arabic_asmo_numbers);
	if (ret)
	{
		return ret;
	}

	return NULLOPT;
}

OPTIONAL<wchar_t> asmo_char_to_arabic(std::string c)
{
	OPTIONAL<wchar_t> ret;

	ret = get_arabic_from_vector(c, arabic_str_asmo_numbers);
	if (ret)
	{
		return ret;
	}

	ret = get_arabic_from_vector(c, arabic_str_asmo_symbols);
	if (ret)
	{
		return ret;
	}
	return NULLOPT;
}

OPTIONAL<wchar_t> asmo_str_to_arabic(std::string c)
{
	OPTIONAL<wchar_t> ret;

	ret = get_arabic_from_vector(c, arabic_str_asmo_numbers); 
	if (ret)
	{
		return ret;
	}

	ret = get_arabic_from_vector(c, arabic_str_asmo_symbols); 
	if (ret)
	{
		return ret;
	}

	return NULLOPT;
}

void replace_all(std::wstring& str, const std::wstring& from, const std::wstring& to)
{

	while (true)
	{
		size_t start_pos = str.find(from);
		if (start_pos == std::wstring::npos)
			break;
		str.replace(start_pos, from.length(), to);
	}
	return;
}

std::wstring prepare_numbers_and_puncs(std::wstring line)
{
	std::wstring ret;
	std::wstring dig = L"";
	for (size_t i = 0; i < line.size(); i++)
	{
		// reverse numbers order and put spaces between them.
		if (is_arabic_digit(line[i]) || is_asmo_digit(line[i]))
		{
			dig = std::wstring(L" ") + line[i] + std::wstring(L" ") + dig;
		}
		else
		{
			ret += dig;
			dig = L"";

			// put spaces before and after every symbol.
			if (is_arabic_symbol(line[i]))
			{
				ret += std::wstring(L" ") + line[i] + std::wstring(L" ");
			}
			else
			{
				ret += line[i];
			}
		}
	}
	ret += dig;
	dig = L"";

	ASMO::replace_all(ret, L"  ", L" ");
	return ret;
}

std::wstring convert_asmo_to_arabic(const std::string& input, const std::wstring& replace)
{
	std::wstring output;
	auto input_str = input; 
	OPTIONAL<wchar_t> str_node = asmo_str_to_arabic(input);
	if (str_node)
	{
	output += str_node.value();
	return output;
	}
	
	int tashkeel_flag=0;
	int shadda_flag = 0, tnween_flag = 0;
	if (input_str.find("shada") != std::string::npos)
	{
		shadda_flag = 1;
	}
	if (input_str.find("skoon") != std::string::npos)
	{
		tashkeel_flag = 1;	
	}
	else if (input.find("fatha") != std::string::npos)
	{
		tashkeel_flag = 2;
		 tnween_flag = input_str.find("tn_") != std::string::npos;
	}
	else if (input_str.find("damma") != std::string::npos)
	{
		tashkeel_flag = 3;
		 tnween_flag = input_str.find("tn_") != std::string::npos;
	}
	else if (input_str.find("kasra") != std::string::npos)
	{
		tashkeel_flag = 4;
		 tnween_flag = input_str.find("tn_") != std::string::npos;
		
	}
	if ((tashkeel_flag) || (shadda_flag))
	{
		input_str = input_str[0];
	}

	

	for (const char& c : input_str)
	{
		if (c == ' ')
		{
			output += L' ';
			continue;
		}
		OPTIONAL<wchar_t> node = asmo_char_to_arabic(c) ;

		if (!node) // not found
		{
			output += replace;
		}
		else
		{
			output += node.value();
		}
	}
	
	if ((tashkeel_flag)||(shadda_flag))
	{
		output = handle_tashkeel(output, tnween_flag, shadda_flag,tashkeel_flag);
	}
	return output;
}

void TrimWord(std::string& word)
{
	if (word.empty())
		return;

	// Trim spaces from left side
	while (word.find(" ") == 0)
	{
		word.erase(0, 1);
	}

	// Trim spaces from right side
	size_t len = word.size();
	while (word.rfind(" ") == --len)
	{
		word.erase(len, len + 1);
	}
}

std::string convert_arabic_to_asmo(const std::wstring& input, const std::string& replace)
{
	std::string output;
	for (const wchar_t& c : input)
	{
		if (c == L' ')
		{
			output += ' ';
			continue;
		}
		OPTIONAL<char> node = arabic_char_to_asmo(c);
		if (!node) // not found
		{
			output += replace;
		}
		else
		{
			output += node.value();
		}
	}
	TrimWord(output);
	return output;
}

bool is_asmo_number(const std::string& word)
{
	if (RDI::ASMO::is_asmo_digit(word))
	{
		return true;
	}

	for(const auto& ch : word)
	{
		if(!RDI::ASMO::is_asmo_digit(ch))
		{
			return false;
		}
	}


	return true;
}

bool is_arabic_number(const std::wstring& word)
{
	for(const auto& ch : word)
	{
		if(!RDI::ASMO::is_arabic_digit(ch))
		{
			return false;
		}
	}
	return true;
}

std::wstring remove_non_alphabetic_arabic_characters(const std::wstring& text)
{
	std::wstring ret;

	for(wchar_t ch : text)
	{
		if(RDI::ASMO::is_arabic_char(ch) || ch == L' ')
		{
			ret += ch;
		}
	}

	return ret;
}

} // namespace ASMO
} // namespace RDI
