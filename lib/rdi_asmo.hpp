#ifndef RDI_ASMO_HPP
#define RDI_ASMO_HPP
#include <map>
#include <iostream>
#include <vector>

namespace RDI
{

namespace ASMO
{

enum class ar : wchar_t
{
	// alphabet
	hamza            = L'\u0621',
	alef_madda       = L'\u0622',
	alef_hamza_above = L'\u0623',
	waw_hamza_above  = L'\u0624',
	alef_hamza_below = L'\u0625',
	yeh_hamza_above  = L'\u0626',
	alef_no_hamza    = L'\u0627',
	beh              = L'\u0628',
	teh_marboota     = L'\u0629',
	teh              = L'\u062A',
	theh             = L'\u062B',
	jeem             = L'\u062C',
	hah              = L'\u062D',
	khah             = L'\u062E',
	dal              = L'\u062F',
	thal             = L'\u0630',
	reh              = L'\u0631',
	zain             = L'\u0632',
	seen             = L'\u0633',
	sheen            = L'\u0634',
	sad              = L'\u0635',
	dad              = L'\u0636',
	tah              = L'\u0637',
	zah              = L'\u0638',
	ain              = L'\u0639',
	ghain            = L'\u063A',
	feh              = L'\u0641',
	qaf              = L'\u0642',
	kaf              = L'\u0643',
	lam              = L'\u0644',
	meem             = L'\u0645',
	noon             = L'\u0646',
	heh              = L'\u0647',
	waw              = L'\u0648',
	yeh_no_dots      = L'\u0649',
	yeh_with_dots    = L'\u064A',

	// symbols						  ,
	fasla          = L'\u060C',
	question_mark  = L'\u061F',
    fasla_mankoota = L'\u061B',
	noqtatan       = L'\u003A',
	nesba          = L'\u0025',
	ta3agob        = L'\u0021',
	qaws_maftooh   = L'\u0028',
	qaws_makfool   = L'\u0029',
	darb           = L'\u002A',
	darb_alt	   = L'\u00D7',
	jam3           = L'\u002B',
	kesma          = L'\u002F',
	tar7           = L'\u002D',
	yosawi         = L'\u003D',
    noqta          = L'\u002E',
    tansees        = L'\u0022',
    exponent       = L'\u065b',
    single_quote_right = L'\u2019',
    single_quote_left  = L'\u2018',
    double_quote_right = L'\u201d',
    double_quote_left  = L'\u201c',
    doubly_angle_bracket_right  = L'\u00bb',
    doubly_angle_bracket_left  = L'\u00ab',

	// numbers
	sefr      = L'\u0660',
	wahed     = L'\u0661',
	ethnan    = L'\u0662',
	thalatha  = L'\u0663',
	arba3a    = L'\u0664',
	khamsa    = L'\u0665',
	setta     = L'\u0666',
	sab3a     = L'\u0667',
	thamaneya = L'\u0668',
	tes3a     = L'\u0669',

	// harakat
	tatweel       = L'\u0640',
	tanween_fatha = L'\u064b',
	tanween_dama  = L'\u064c',
	tanween_kasra = L'\u064d',
	fatha         = L'\u064e',
	dama          = L'\u064f',
	kasra         = L'\u0650',
	shada         = L'\u0651',
	skoon         = L'\u0652',


};
#define STRINGIFY(x) std::wstring(1, static_cast<wchar_t>(x))

extern const std::vector<std::pair<wchar_t, char>> arabic_asmo_numbers;
extern const std::vector<std::pair<wchar_t, char>> arabic_asmo_symbols;
extern const std::vector<std::pair<wchar_t, char>> arabic_asmo_chars;
extern const std::vector<std::pair<wchar_t, std::string>> arabic_str_asmo_numbers;
extern const std::vector<std::pair<wchar_t, std::string>> arabic_str_asmo_symbols;

/// call this before calling convert_arabic_to_asmo to handle numbers and punctuation
std::wstring prepare_numbers_and_puncs(std::wstring line);

std::wstring convert_asmo_to_arabic(const std::string&,const std::wstring& replace=L"");
std::string convert_arabic_to_asmo(const std::wstring&,const std::string& replace="");
bool is_arabic_char(wchar_t c);
bool is_arabic_digit(wchar_t c);
bool is_arabic_symbol(wchar_t c);
bool is_asmo_symbol(char c);
bool is_asmo_char(wchar_t c);
bool is_asmo_digit(wchar_t c);

bool is_asmo_number(const std::string& text);
bool is_arabic_number(const std::wstring& txt);

// remove any char that is not arabic or space.
std::wstring remove_non_alphabetic_arabic_characters(const std::wstring& text);

} // namespace ASMO
} // namespace RDI
#endif // RDI_ASMO_HPP
